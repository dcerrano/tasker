import { ClienteService, DepartamentoService } from './../../servicios';
import { Departamento } from './../../modelos';
import { GenericForm } from './../../generic-form.component';
import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../modelos';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css'],
  providers:[ClienteService, DepartamentoService]
})
export class ClienteFormComponent extends GenericForm<Cliente> {
  //para mostrar en select
  departamentos: Departamento[];


  constructor(
    protected clienteService: ClienteService,
    protected depService: DepartamentoService) {
    super(clienteService);
    this.cargarSelects();
  }

  cargarSelects() {
    this.depService.findAll().subscribe(resp => {
      this.departamentos = resp;
    });
  }

  appendFormData(fd) {
    fd.append('ruc', this.entity.ruc);
    fd.append('razonSocial', this.entity.razonSocial);
    fd.append('departamento.id', this.entity.departamento.id);
  }
}
