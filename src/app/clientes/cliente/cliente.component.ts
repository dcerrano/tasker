import { ClienteListComponent } from './../cliente-list/cliente-list.component';
import { ClienteFormComponent } from './../cliente-form/cliente-form.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  @ViewChild(ClienteListComponent) lista: ClienteListComponent;
  @ViewChild(ClienteFormComponent) form: ClienteFormComponent;
  @ViewChild("formTab") formTab: ElementRef;

  constructor() { }

  ngOnInit() {
    /*Al hacer click en la tabla, cargar formulario y cambiar tab*/
    this.lista.getSelectedDataSubject().subscribe(fila => {
      console.log("ClienteComponent -> Registro seleccionado:", fila);
      this.form.setEntity(fila);
      this.formTab.nativeElement.click();
    });

    /*Al borrar o agregar registro en formulario, actualizar lista */
    this.form.getEntityChangedSubject().subscribe(entity => {
      this.lista.refresh();
    })
  }

}
