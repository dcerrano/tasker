import { TareaFormComponent } from './../tareas/tarea-form/tarea-form.component';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Tarea } from '../modelos';
import { TareaService } from '../servicios';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[TareaService]
})
export class DashboardComponent implements OnInit {
  tareasPendientes: Tarea[] = [];
  tareasFinalizadas: Tarea[] = [];
  tareaSeleccionada: Tarea;
  estado: string;
  @ViewChild("closeTareaFormModalBtn") closeTareaFormModalBtn: ElementRef;
  @ViewChild(TareaFormComponent) tareaForm : TareaFormComponent

  constructor(private tareaService: TareaService) {
    
  }

  ngOnInit() {
    this.recargarTablas();
    /*Desde el dashboard se puede registrar una tarea, después de guardar se debe actualizar lista*/
    this.tareaForm.getEntityChangedSubject().subscribe(resp=>{
          this.recargarTablas();
          //cerrar modal
          this.closeTareaFormModalBtn.nativeElement.click()
    })
  }

  recargarTablas() {
    this.tareaService.getTareasPendientes().subscribe(resp => {
      this.tareasPendientes = resp;
      console.log('Tareas pendientes:', this.tareasPendientes);
    });


    this.tareaService.getTareasFinalizadas().subscribe(resp => {
      this.tareasFinalizadas = resp;
      console.log('Tareas finalizadas:', this.tareasFinalizadas);
    });
  }
  
  setFinalizada() {
    console.log("Finalizar tarea:", this.tareaSeleccionada);
    this.tareaService
      .setFinalizada(this.tareaSeleccionada).subscribe(resp => {
        this.recargarTablas();
      });
  }

  setPendiente() {
    console.log("Set pendiente:", this.tareaSeleccionada);
    this.tareaService
      .setPendiente(this.tareaSeleccionada).subscribe(resp => {
        this.recargarTablas();
      });
  }

  cambiarEstado() {
    if (this.estado == 'F') {
      this.setFinalizada();
    } else {
      this.setPendiente();
    }
  }

  seleccionarTarea(tarea, estado) {
    console.log("Tarea seleccionada:", tarea);
    this.tareaSeleccionada = tarea;
    this.estado = estado;
  }
}
