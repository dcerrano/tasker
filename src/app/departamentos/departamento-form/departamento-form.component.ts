import { Component } from '@angular/core';
import { GenericForm } from './../../generic-form.component';
import { Departamento } from './../../modelos';
import { DepartamentoService } from './../../servicios';

@Component({
  selector: 'app-departamento-form',
  templateUrl: './departamento-form.component.html',
  styleUrls: ['./departamento-form.component.css'],
  providers: [DepartamentoService]
})
export class DepartamentoFormComponent extends GenericForm<Departamento> {
  constructor(protected service: DepartamentoService) {
    super(service);
  }

  appendFormData(fd: FormData){
     fd.append('nombre', this.entity.nombre);
  }
}
