import { Subject } from 'rxjs';
import { Global } from './../../globals';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Departamento } from '../../modelos';
import { DepartamentoService } from '../../servicios';

@Component({
  selector: 'app-departamento-list',
  templateUrl: './departamento-list.component.html',
  styleUrls: ['./departamento-list.component.css'],
  providers:[DepartamentoService]
})
export class DepartamentoListComponent implements OnInit {
  departamentos: Departamento[] = []
  selectedDataSubject = new Subject();

  constructor(private depService: DepartamentoService) {

  }

  ngOnInit() {
    this.depService.findAll().subscribe(resp => {
      this.departamentos = resp;
    });
  }

  selectData(d) {
    console.log("Fila seleccionada:", d);
    this.selectedDataSubject.next(d);
  }
  getSelectedDataSubject() {
    return this.selectedDataSubject.asObservable();
  }

}
