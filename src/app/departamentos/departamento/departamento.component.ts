import { DepartamentoFormComponent } from './../departamento-form/departamento-form.component';
import { DepartamentoListComponent } from './../departamento-list/departamento-list.component';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.css']
})
export class DepartamentoComponent implements OnInit {

  @ViewChild(DepartamentoListComponent) 
     lista: DepartamentoListComponent;

  @ViewChild(DepartamentoFormComponent) 
     form: DepartamentoFormComponent;


  constructor() { }

  ngOnInit() {
      this.lista.getSelectedDataSubject().subscribe(d =>{
          this.form.setEntity(d);
      });
  }

}
