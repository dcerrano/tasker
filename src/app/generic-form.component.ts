import { AbmButtonsComponent } from './shared/abm-buttons/abm-buttons.component';
import { GenericService } from './generic.service';
import { ViewChild, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';

export class GenericForm<T> implements OnInit {
    @ViewChild('myForm') myForm: NgForm;
    @ViewChild(AbmButtonsComponent) abmButtons: AbmButtonsComponent;
    entity: T;
    errorMsg: string;
    okMsg: string;

    entityChangedSubject = new Subject();

    constructor(protected service: GenericService<T>) {

    }

    ngOnInit() {
        this.abmButtons.setForm(this);
        this.newEntity();
    }

    save() {
        console.log('guardando registro:', this.entity);
        const formData = new FormData();
        if (this.entity['id']) {
            formData.append('id', this.entity['id']);
        }
        this.appendFormData(formData);
        this.service.save(formData).subscribe(respOk => {
            this.okMsg = 'Registro guardado con éxito';
            this.errorMsg = null;
            console.log(this.okMsg, respOk);
            this.entityChangedSubject.next(this.entity);
        }, respError => {
            this.errorMsg = 'Error al guardar registro: ' + respError.error.errorMsg;
            this.okMsg = null;
            console.error(this.errorMsg, respError);
        });
    }

    appendFormData(formData) {

    }

    setEntity(data) {
        console.log('GenericForm.setEntity:', data);
        this.entity = data;
        /*Mostrar todos los botones al seleccionar un registro */
        this.abmButtons.deleteButtonEnabled = true;
        this.abmButtons.newButtonEnabled = true;
        this.abmButtons.saveButtonEnabled = true;
        this.okMsg = 'Actualizar registro';
        this.errorMsg = null;
    }

    newEntity() {
        this.okMsg = 'Complete los campos';
        this.errorMsg = null;
        this.entity = {} as T;
        this.abmButtons.deleteButtonEnabled = false;
        this.abmButtons.newButtonEnabled = false;
        this.abmButtons.deleteButtonEnabled = false;
        this.abmButtons.saveButtonEnabled = true;
    }

    deleteEntity() {
        console.log("Borrando registro:", this.entity);
        this.service.delete(this.entity['id'])
            .subscribe(
                resp => {
                    console.log("Registro", this.entity, "borrado");
                    this.okMsg = 'Registro borrado';
                    this.entityChangedSubject.next(this.entity);
                    this.entity = {} as T;
                    this.abmButtons.deleteButtonEnabled = false;
                    this.abmButtons.newButtonEnabled = false;
                    this.abmButtons.saveButtonEnabled = true;
                },
                resp => {
                    console.log("No se pudo borrar", resp);
                    this.errorMsg = 'No se pudo borrar registro. '
                     + resp.error.errorMsg
                });
    }

    compareEntityFn(item, selected) {
        if (selected) {
            return item.id == selected.id;
        }
        return false;
    }

    getEntityChangedSubject() {
        return this.entityChangedSubject.asObservable();
    }
}