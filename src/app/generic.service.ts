import { HttpClient } from '@angular/common/http';
import { Global } from './globals';

export class GenericService<T> {

    constructor(protected http: HttpClient,
        protected mapping: string) {
    }

    findAll() {
        return this.http.get<T[]>(Global.API_URL + this.mapping);
    }

    save(formData) {
        return this.http.post(Global.API_URL
            + this.mapping, formData);
    }
    //http://45.56.88.72/owlback/departamentos/7
    delete(id) {
        return this.http.delete(Global.API_URL
            + this.mapping +'/'+id);
    }
}