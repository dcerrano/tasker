import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpErrorResponse
} from '@angular/common/http';

import 'rxjs/add/observable/throw';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (1==1) {
      return true;
    }

    // not logged in so redirect to login page with the return url and return false
    this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

}


@Injectable()
export class CustomInterceptor implements HttpInterceptor {
  intentos: number = 0;
  constructor(private injector: Injector, private router: Router ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<
  | HttpSentEvent
  | HttpHeaderResponse
  | HttpProgressEvent
  | HttpResponse<any>
  | HttpUserEvent<any>| HttpEvent<any> > {


    request = request.clone({
      withCredentials: true
    });

    return next.handle(request).catch(error => {
      if ((error instanceof HttpErrorResponse)) {
        console.log("tipo de error en el interceptor", (<HttpErrorResponse>error).status, error);
        switch ((<HttpErrorResponse>error).status) {
          case 401:
            if(request.url.indexOf("/login")<0){
                this.router.navigate(['login'], { queryParams: { returnUrl: location.pathname } });
            }
            return Observable.throw(new HttpErrorResponse({ status:401, error: 'Usuario no autenticado' }));
          case 400:
          return Observable.throw(new HttpErrorResponse({ error: 'Errores de validación.', status:400 }));
          case 0:
          return Observable.throw(new HttpErrorResponse({ error: 'Verifique su conexión e intente de nuevo.' }));
          case 500:
            return Observable.throw(new HttpErrorResponse({ error: 'Problemas en el servidor.' }));
            default:
            return Observable.throw(error);
        }
      } else {
        return Observable.throw(error);
      }
    });
  }

}