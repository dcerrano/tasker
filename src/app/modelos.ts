
export class Usuario {
    codigo: string
    nombres: string
    apellidos: string
}

export class SesionInfo {
    usuario: Usuario
}

export class Departamento {
    id: number;
    nombre: string;

    constructor(nombre?: string) {
        this.nombre = nombre;
    }
}

export class Cargo {
    id: number;
    nombre: string;
}

export class Solicitante {
    id: number;
    cedula: string;
    nombre: string;
    apellido: string;
    cargo: Cargo;
    departamento: Departamento;

    constructor(id: number, cedula?: string, nombre?: string, apellido?: string) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }
}

export class TipoTarea {
    id: number
    nombre: string
}

export class Tarea {
    id: number;
    solicitante: Solicitante;
    tipoTarea: TipoTarea
    fechaSolicitud: string
    estado: string
    observacion: string;
}


export class Personal {
    id: number;
    codigo: string;
    nombre: string;
    apellido: string;
    observacion: string;
}

export class Cliente {
    id: number
    ruc: string
    razonSocial: string
    departamento: Departamento
    direccion: string
}