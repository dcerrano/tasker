import { Component, OnInit } from '@angular/core';
import { GenericForm } from '../../generic-form.component';
import { Personal } from '../../modelos';
import { PersonalService } from '../../servicios';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-personal-form',
  templateUrl: './personal-form.component.html',
  styleUrls: ['./personal-form.component.css'],
  providers:[PersonalService]
})
export class PersonalFormComponent extends GenericForm<Personal> {


  constructor(private servicio: PersonalService) { 
    super(servicio);
  }

  appendFormData(formData: FormData) {
      formData.append('codigo', this.entity.codigo);
      formData.append('nombre', this.entity.nombre);
      formData.append('apellido', this.entity.apellido);
  }
}
