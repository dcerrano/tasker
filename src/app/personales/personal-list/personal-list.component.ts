import { PersonalService } from './../../servicios';
import { Personal } from './../../modelos';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-personal-list',
  templateUrl: './personal-list.component.html',
  styleUrls: ['./personal-list.component.css'],
  providers: [PersonalService]
})
export class PersonalListComponent implements OnInit {

  personales: Personal[] = [];
  selectedDataSubject = new Subject();

  constructor(private service: PersonalService) { }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.service.findAll().subscribe(resp => {
      this.personales = resp;
    });
  }

  selectData(data) {
    console.log("PersonalList.selectData:", data);
    this.selectedDataSubject.next(data);
  }

  getSelectedDataSubject() {
    return this.selectedDataSubject.asObservable();
  }

}
