import { PersonalFormComponent } from './../personal-form/personal-form.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PersonalListComponent } from '../personal-list/personal-list.component';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  @ViewChild(PersonalListComponent) list : PersonalListComponent;
  @ViewChild(PersonalFormComponent) form : PersonalFormComponent;
  @ViewChild("formTab") formTab : ElementRef;
  constructor() { }

  ngOnInit() {
    this.list.getSelectedDataSubject().subscribe(resp => {
         this.form.setEntity(resp);
         this.formTab.nativeElement.click();
    });

    this.form.getEntityChangedSubject().subscribe(entidad =>{
       this.list.refresh();
    });
  }

}
