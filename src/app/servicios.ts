import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Departamento, Cargo, Solicitante, Personal, TipoTarea, Tarea, Cliente, SesionInfo } from './modelos';
import { GenericService } from './generic.service';
import { Injectable } from '@angular/core';
import { Global } from './globals';


@Injectable()
export class SesionService {
    private sesionInfo: SesionInfo
    
    constructor(protected http: HttpClient) {

    }

    getUsuario() {
        if (this.sesionInfo) {
            return this.sesionInfo.usuario
        }
        return null
    }

    setSesionInfo(info) {
        this.sesionInfo = info
    }


    public checkSession(): Observable<SesionInfo> {
        return this.http.get<SesionInfo>(Global.API_URL + 'sesion/info');
    }


    public login(credentials): Observable<SesionInfo> {
        if (credentials.username === null || credentials.password === null) {
            return Observable.throw("Por favor ingrese sus credenciales");
        } else {
            let formData: FormData = new FormData();
            formData.append('username', credentials.username);
            formData.append('password', credentials.password);
            return this.http.post<SesionInfo>(Global.API_URL + 'login', formData);
        }
    }


    public logout(): Observable<any> {
        return this.http.post(Global.API_URL + 'cerrar-sesion', '');
    }
}


@Injectable()
export class DepartamentoService
    extends GenericService<Departamento>{
    constructor(protected http: HttpClient) {
        super(http, 'departamentos');
    }
}


@Injectable()
export class CargoService
    extends GenericService<Cargo>{
    constructor(protected http: HttpClient) {
        super(http, 'cargos');
    }
}


@Injectable()
export class SolicitanteService
    extends GenericService<Solicitante>{
    constructor(protected http: HttpClient) {
        super(http, 'solicitantes');
    }
}

@Injectable()
export class PersonalService
    extends GenericService<Personal>{
    constructor(protected http: HttpClient) {
        super(http, 'personales');
    }
}


@Injectable()
export class TareaService
    extends GenericService<Tarea>{
    constructor(protected http: HttpClient) {
        super(http, 'tareas');
    }

    getTareasPendientes() {
        return this.http.get<Tarea[]>(Global.API_URL + 'tareas/pendientes');
    }

    getTareasFinalizadas() {
        return this.http.get<Tarea[]>(Global.API_URL + 'tareas/finalizadas');
    }

    setFinalizada(tarea: Tarea) {
        return this.http.post(Global.API_URL
            + 'tareas/set-finalizada/' + tarea.id, null);
    }

    setPendiente(tarea: Tarea) {
        return this.http.post(Global.API_URL
            + 'tareas/set-pendiente/' + tarea.id, null);
    }
}


@Injectable()
export class TipoTareaService
    extends GenericService<TipoTarea>{
    constructor(protected http: HttpClient) {
        super(http, 'tipos-tareas');
    }
}


@Injectable()
export class ClienteService
    extends GenericService<Cliente>{
    constructor(protected http: HttpClient) {
        super(http, 'clientes');
    }
}