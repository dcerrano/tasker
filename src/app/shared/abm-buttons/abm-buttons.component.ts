import { GenericForm } from './../../generic-form.component';
import { FormEvent } from './form.events';
import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { SubjectSubscriber } from 'rxjs/internal/Subject';


@Component({
  selector: 'app-abm-buttons',
  templateUrl: './abm-buttons.component.html',
  styleUrls: ['./abm-buttons.component.css']
})
export class AbmButtonsComponent implements OnInit {

  newButtonEnabled = true;
  saveButtonEnabled = true;
  deleteButtonEnabled = true;
  //se debe asignar el formulario que se va a utilizar
  form: GenericForm<Object>;

  ngOnInit() {

  }
  setForm(form) {
    this.form = form;
  }
  onNewButtonClicked() {
    console.log("Click en botón nuevo")
    this.form.newEntity();
  }

  onSaveButtonClicked() {
    console.log("Click en botón guardar")
    this.form.save();
  }

  onDeleteButtonClicked() {
    console.log("Click en botón borrar")
    this.form.deleteEntity();
  }
}
