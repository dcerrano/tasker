import { Usuario } from './../../modelos';
import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../servicios';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private sesionService: SesionService, private router: Router) { }

  usuario : Usuario
  ngOnInit() {
     this.usuario = this.sesionService.getUsuario();
     if(!this.usuario) {
       this.sesionService.checkSession().subscribe(resp => {
         this.sesionService.setSesionInfo(resp)
         this.usuario = this.sesionService.getUsuario();
       });
     }
  }

  logout() {
    this.sesionService.logout().subscribe(resp => {
      this.router.navigate(['login']);
    })
  }

}
