import { Component } from '@angular/core';
import { GenericForm } from './../../generic-form.component';
import { Cargo, Departamento, Solicitante } from './../../modelos';
import { CargoService, DepartamentoService, SolicitanteService } from './../../servicios';

@Component({
  selector: 'app-solicitante-form',
  templateUrl: './solicitante-form.component.html',
  styleUrls: ['./solicitante-form.component.css'],
  providers: [DepartamentoService, CargoService, SolicitanteService]
})
export class SolicitanteFormComponent extends GenericForm<Solicitante> {
  //para mostrar en select
  departamentos: Departamento[];
  cargos: Cargo[];

  constructor(
    protected solService: SolicitanteService,
    protected depService: DepartamentoService,
    protected cargoService: CargoService) {
    super(solService);
    this.cargarSelects();
  }

  cargarSelects() {
    this.depService.findAll().subscribe(resp => {
      this.departamentos = resp;
    });

    this.cargoService.findAll().subscribe(resp => {
      this.cargos = resp;
    });
  }

  appendFormData(fd) {
    fd.append('cedula', this.entity.cedula);
    fd.append('nombre', this.entity.nombre);
    fd.append('apellido', this.entity.apellido);
    fd.append('cargo.id', this.entity.cargo.id +'');
    fd.append('departamento.id', this.entity.departamento.id);
  }
}
