import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Solicitante } from '../../modelos';
import { SolicitanteService } from '../../servicios';

@Component({
  selector: 'app-solicitante-list',
  templateUrl: './solicitante-list.component.html',
  styleUrls: ['./solicitante-list.component.css'],
  providers: [SolicitanteService]
})
export class SolicitanteListComponent implements OnInit {

  solicitantes: Solicitante[] = []
  selectedDataSubject = new Subject();

  constructor(private solService: SolicitanteService) {
    this.refresh()
  }

  ngOnInit() {
  }

  refresh() {
    this.solService.findAll().subscribe(resp => {
      this.solicitantes = resp;
    });
  }

  selectData(data) {
    console.log('SolicitanteListComponent.selectData: ', data);
    this.selectedDataSubject.next(data);
  }

  getSelectedDataSubject() {
    return this.selectedDataSubject.asObservable();
  }

}
