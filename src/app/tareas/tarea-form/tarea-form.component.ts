import { SolicitanteService, TipoTareaService, TareaService } from './../../servicios';
import { GenericForm } from './../../generic-form.component';
import { Tarea, TipoTarea, Solicitante } from './../../modelos';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tarea-form',
  templateUrl: './tarea-form.component.html',
  styleUrls: ['./tarea-form.component.css'],
  providers:[TipoTareaService, SolicitanteService, TareaService]
})
export class TareaFormComponent extends GenericForm<Tarea> {
  //para mostrar en select
  tipoTareaList: TipoTarea[];
  solicitanteList: Solicitante[];

  constructor(
    private tareaService:TareaService,
    private solService: SolicitanteService,
    private tipoTareaService: TipoTareaService) {
    super(tareaService);
    this.cargarSelects();
  }

  cargarSelects() {
    this.tipoTareaService.findAll().subscribe(resp => {
      this.tipoTareaList = resp;
    });

    this.solService.findAll().subscribe(resp => {
      this.solicitanteList = resp;
    });
  }

  appendFormData(fd) {
    fd.append('tipoTarea.id', this.entity.tipoTarea.id);
    fd.append('solicitante.id', this.entity.solicitante.id);
    fd.append('observacion', this.entity.observacion);
  }
}
