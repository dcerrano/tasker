import { Component, OnInit } from '@angular/core';
import { Tarea } from '../../modelos';
import { Subject } from 'rxjs';
import { TareaService } from '../../servicios';

@Component({
  selector: 'app-tarea-list',
  templateUrl: './tarea-list.component.html',
  styleUrls: ['./tarea-list.component.css'],
  providers:[TareaService]
})
export class TareaListComponent implements OnInit {

  tareas: Tarea[] = [];

  selectedDataSubject = new Subject();

  constructor(private service: TareaService) {
    this.refresh()
  }

  ngOnInit() {
  }

  refresh() {
    this.service.findAll().subscribe(resp => {
      this.tareas = resp;
    });
  }

  selectData(data) {
    console.log('TareaListComponent.selectData: ', data);
    this.selectedDataSubject.next(data);
  }

  getSelectedDataSubject() {
    return this.selectedDataSubject.asObservable();
  }
}
